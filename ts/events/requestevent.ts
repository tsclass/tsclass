export interface IEvent_Request {
  httpMethod: string;
  httpRoute: string;
  durationToResponse: number;
}
