export type TStatus = 'ok' | 'partly_ok' | 'not_ok';
export interface IStatusObject {
  id?: string;
  name: string;
  combinedStatus?: TStatus;
  combinedStatusText: string;
  details: Array<{
    name: string;
    value: string;
    status: TStatus;
    statusText: string;
  }> 
}