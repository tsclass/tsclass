export interface ICommitInfo {
  name: string;
  version: string;
  description: string;
}
