export interface ISshKey {
  keyName: string;
  public: string;
  private?: string;
}