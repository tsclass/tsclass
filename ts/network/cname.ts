import { type ICert } from "./cert.js";

export class ICnameDescriptor {
  /**
   * the original domain that is being cnamed
   */
  cnamedDomain: string;

  /**
   * the cname proxy domain
   * the domain that is used for the cname dns entry
   */
  cnameProxyDomain: string;

  /**
   * the internal target url
   */
  targetUrl: string;

  /**
   * a description
   */
  description: string;

  /**
   * the ssl certificate
   */
  sslCertificate?: ICert;
};