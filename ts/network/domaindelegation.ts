export interface IDomainDelegation {
  /**
   * only if it applis: the origininating url
   */
  fullUrl: string;
  /**
   * the full domain name
   */
  fullDomain: string;
  /**
   * the domain, meaning whats usually considered a domain like google.com
   */
  domain: string;
  /**
   * the public suffix, meaning whats usually considered a public suffix like .com
   */
  publicSuffix: string;

  /**
   * the domain without the public suffix
   */
  domainWithoutSuffix: string;

  /**
   * the subdomain, meaning whats usually considered a subdomain like www
   */
  subdomain: string;

  isIcann?: boolean;
  dnsSecEnabled?: boolean;
}
