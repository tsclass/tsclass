export interface IJwtKeypair {
  privatePem: string;
  publicPem: string;
}