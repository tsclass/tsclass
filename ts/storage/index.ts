export interface IS3Descriptor {
  endpoint: string;
  port?: number;
  useSsl?: boolean;
  accessKey: string;
  accessSecret: string;
}
