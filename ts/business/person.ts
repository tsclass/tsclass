import { type IContact } from './contact.js';

export interface IPerson {
  title: string;
  name: string;
  surname: string;
  sex: 'male' | 'female' | 'queer';
  legalProxyFor?: {
    type: 'self' | 'other';
    contact?: IContact;
  };
}
