import * as finance from '../finance/index.js';
import { type ICompany } from './company.js';
import { type IContact } from './contact.js';
export class IJob {
  type: 'contract' | 'employment';
  techTags?: string[];
  qualificationTags?: string[];
  languages?: {
    name: string;
    level: 'basic' | 'intermediate' | 'advanced' | 'native';
  }[];
  name: string;
  description: string;
  monthlyTotal: number;
  currency: finance.TCurrency;
  from: ICompany;
  contact: IContact;
}