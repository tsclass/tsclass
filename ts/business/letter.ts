import * as business from './index.js';
import * as finance from '../finance/index.js';
import * as database from '../database/index.js';
export interface ILetter {
  versionInfo: {
    type: 'draft' | 'final';
    /**
     * should follow semVer
     */
    version: string;
  };
  incidenceId: string;
  type: 'invoice' | 'notice' | 'warning' | 'verification' | 'contract';
  date: number;
  from: business.IContact;
  to: business.IContact;
  legalContact: business.IContact;
  logoUrl: string;
  subject: string;
  accentColor?: string;
  needsCoverSheet: boolean;
  content: {
    textData: string[];
    invoiceData?: finance.IInvoice;
    contractData?: {
      id: string;
      contractDate: number;
    };
    timesheetData: string;
  }
  pdf?: business.IPdf;
  pdfAttachments: business.IPdf[];
  language: string;
  objectActions: database.IObjectAction[];
}
