import * as database from '../database/index.js';
import { type IPerson } from './person.js';

export interface IContract {
  parties: {
    signingOrder: number;
    referencedAs: string;
    person: IPerson;
    role: 'signer' | 'cc';
    signature: {
      given: boolean;
      timestamp?: number;
      location?: string;
      ip?: string;
      verifications?: [];
    };
  }[];
  contractTextMarkdown: string;
  actions: database.IObjectAction[];
}
