export interface IProject {
  active: boolean;
  category: 'SaaS' | 'IaaS' | 'Media' | 'Blockchain' | 'Open Source' | 'Consulting' | 'internal' | 'partner';
  branch?: null;
  branchId? : string;
  domain: string;
  redirectDomains: string[];
  gitlab: string;
  name: string;
  social: {
    facebook?: string;
    gitlab?: string;
    github?: string;
    twitter?: string;
  };
  tagLine: string;
  tags: string[];
}