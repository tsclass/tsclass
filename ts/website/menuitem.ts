export interface IMenuItem<T = any, U = any> {
  name: string;
  iconName?: string;
  action: (optionsArg?: T) => Promise<U>;
}
