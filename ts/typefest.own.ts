type SecondArgument<T> = T extends (arg1: any, arg2: infer P, ...args: any[]) => any ? P : never;

type ValueType<T> = T extends { [key: string]: infer U } ? U : never;

export type {
  SecondArgument,
  ValueType
}