export interface IWrappedData<T> {
  data: T;
}
