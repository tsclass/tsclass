export interface IObjectStatus {
  current: 'active' | 'inactive' | 'hidden' | 'markedForDeletion';
  scheduledDeletion: number;
  justForLooks: {
    scheduledDeletionIso: string;
  };
}
