export interface IObjectAction {
  timestamp: number;
  name: string;
  userId: string;
  data: any;
  message: string;
  privateMessage: string;
}
