/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@tsclass/tsclass',
  version: '4.0.52',
  description: 'common classes for TypeScript'
}
