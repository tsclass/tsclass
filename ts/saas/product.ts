import { type ICompany } from "../business/company.js";

export interface IProduct {
  name: string;
  slogan: string;
  description: string;
  os: 'web-based',
  category: 'Business Application',
  offers: any[];
  landingPage: string;
  appLink: string;
  logoLink: string;
  publisher?: ICompany;
}